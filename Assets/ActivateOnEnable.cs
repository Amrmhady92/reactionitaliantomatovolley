﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnEnable : MonoBehaviour {

	public GameObject[] activatedObject;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnEnable()
	{
		for (int i = 0; i < activatedObject.Length; i++)
		{
			activatedObject[i].SetActive(true);

		}
	}
}
